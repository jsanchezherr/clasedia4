package com.nttdata.clase02.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.nttdata.clase02.entity.Persona;
import com.nttdata.clase02.respository.PersonaRespository;
import com.nttdata.clase02.service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PersonaService implements IPersonaService {

  @Autowired
  private PersonaRespository personaDao;

  @Override
  public List<Persona> listAll() {
    return this.personaDao.findAll();
  }

  @Override
  public List<Persona> listPersonasMayoresCinco() {


    return this.personaDao.findAll().stream()
                          .filter(p -> p.getEdad() > 5)
                          .collect(Collectors.toList());
  }

  @Override
  public Persona obtenerPersona(int id) {
    return null;
  }

  @Override
  public void deletePersona(int id) {

  }

  @Override
  public void saveOrUpdatePersona(Persona persona) {

  }

  @Override
  public Page<Persona> listAll(Pageable pageable) {
    return null;
  }

}
