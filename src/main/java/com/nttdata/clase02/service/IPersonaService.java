package com.nttdata.clase02.service;

import java.util.List;

import com.nttdata.clase02.entity.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPersonaService {

  List<Persona> listAll();

  List<Persona> listPersonasMayoresCinco();

  Persona obtenerPersona(int id);

  void deletePersona(int id);

  void saveOrUpdatePersona(Persona persona);

  Page<Persona> listAll(Pageable pageable);


}
