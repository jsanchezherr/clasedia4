package com.nttdata.clase02.respository;

import com.nttdata.clase02.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRespository extends JpaRepository<Persona, Integer> {

//  @Query("select p from Persona p where p.dni = :dni and p.edad > 5")
//  Persona obtenerPersona(@Param("dni") String dni);
//
//  Persona findByDni(String dni);

}
