package com.nttdata.clase02.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;

@Entity
@Table(name = "tb_persona")
@Data
public class Persona implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "nombres", nullable = false, length = 50)
  private String nombres;

  @Column(name = "apellido_paterno", nullable = false, length = 80)
  private String apellidoPaterno;

  @Column(name = "apellido_materno", nullable = false, length = 80)
  private String apellidoMaterno;

  @Column(name = "dni", nullable = false, length = 8)
  private String dni;

//  @Transient
  private int edad;

}
